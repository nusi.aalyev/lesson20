FROM golang:alpine
WORKDIR /app
COPY . .
RUN apk add --no-cache postgresql-client curl jq python3 py3-pip
RUN pip install awscli
RUN chmod +x wait-for-postgres.sh
RUN go build -o main main.go
EXPOSE 8080
CMD ["./main"]