#!/bin/sh
set -e

host="$1"
shift
cmd="$@"

aws configure set aws_access_key_id $AWS_KEY_ID \
&& aws configure set aws_secret_access_key $AWS_SECKET_KEY \
&& aws configure set default.region $AWS_REGION

until PGPASSWORD=$POSTGRES_PASSWORD psql -h "$host" -U "postgres" -p 31549  -c '\q'; do
    >&2 echo "Postgres is unavailable - sleeping"
    sleep 1
done

>&2 echo "Postgres is up"
exec $cmd