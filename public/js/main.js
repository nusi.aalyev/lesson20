 async function deleteUser(button) {
  var row = button.parentNode.parentNode;
  var InstanceId = row.getElementsByTagName('td')[0].innerText;
  var xhr = new XMLHttpRequest();
  xhr.open('POST', '/api/ec2/terminate', true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify({ InstanceId: InstanceId }));
  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4 && xhr.status === 200) {
      alert("Terminated instance with id: " +InstanceId);
    }
  };
}