package main

import (
	"fmt"
	"lesson20/db"
	"log"
	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

type ec2Instance struct {
	InstanceId string
}

func DescribeKeyPairs(client *ec2.EC2) (*ec2.DescribeKeyPairsOutput, error) {
	result, err := client.DescribeKeyPairs(nil)
	if err != nil {
		return nil, err
	}

	return result, err
}

func CreateInstance(client *ec2.EC2, imageId string, minCount int, maxCount int, instanceType string, keyName string) (*ec2.Reservation, error) {
	res, err := client.RunInstances(&ec2.RunInstancesInput{
		ImageId:      aws.String(imageId),
		MinCount:     aws.Int64(int64(minCount)),
		MaxCount:     aws.Int64(int64(maxCount)),
		InstanceType: aws.String(instanceType),
		KeyName:      aws.String(keyName),
	})

	if err != nil {
		return nil, err
	}

	return res, nil
}

func TerminateInstance(client *ec2.EC2, instanceId string) error {
	_, err := client.TerminateInstances(&ec2.TerminateInstancesInput{
		InstanceIds: []*string{&instanceId},
	})

	return err
}

func create(c *gin.Context) {

	sess, err := session.NewSessionWithOptions(session.Options{
		Profile: "default",
		Config: aws.Config{
			Region: aws.String("eu-central-1"),
		},
	})

	if err != nil {
		fmt.Printf("Failed to initialize new session: %v", err)
		return
	}

	ec2Client := ec2.New(sess)

	keyName := "aws_key.pub"
	instanceType := "t2.micro"
	minCount := 1
	maxCount := 1
	imageId := "ami-0d1ddd83282187d18"
	newInstance, err := CreateInstance(ec2Client, imageId, minCount, maxCount, instanceType, keyName)
	if err != nil {
		fmt.Printf("Couldn't create new instance: %v", err)
		return
	}

	fmt.Printf("Created new instance: %v\n", *newInstance.Instances[0].InstanceId)
	d, e := db.ConnectDB()

	if e != nil {
		log.Fatal(e)
	}

	sql := "INSERT INTO instance (id_instance) VALUES ($1)"

	_, e = d.Exec(sql, *newInstance.Instances[0].InstanceId)

	if e != nil {
		log.Fatal(e)
	}
	defer d.Close()

	c.Redirect(http.StatusMovedPermanently, "/")

}

func delete(c *gin.Context) {
	var ww ec2Instance
	e := c.BindJSON(&ww)
	if e != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request"})
		return
	}
	sess, err := session.NewSessionWithOptions(session.Options{
		Profile: "default",
		Config: aws.Config{
			Region: aws.String("eu-central-1"),
		},
	})

	if err != nil {
		fmt.Printf("Failed to initialize new session: %v", err)
		return
	}

	ec2Client := ec2.New(sess)

	err = TerminateInstance(ec2Client, ww.InstanceId)
	if err != nil {
		fmt.Printf("Couldn't terimate instance: %v", err)
	}
	d, e := db.ConnectDB()

	if e != nil {
		log.Fatal(e)
	}

	sql := "DELETE FROM instance WHERE id_instance=$1"

	_, e = d.Exec(sql, ww.InstanceId)

	if e != nil {
		log.Fatal(e)
	}
	defer d.Close()

	fmt.Println("Terminated instance with id: ", ww.InstanceId)

	c.Redirect(http.StatusMovedPermanently, "/")
	c.Redirect(http.StatusMovedPermanently, "/")

}

func listKey(c *gin.Context) {
	sess, err := session.NewSessionWithOptions(session.Options{
		Profile: "default",
		Config: aws.Config{
			Region: aws.String("eu-central-1"),
		},
	})

	if err != nil {
		fmt.Printf("Failed to initialize new session: %v", err)
		return
	}

	ec2Client := ec2.New(sess)

	keyPairRes, err := DescribeKeyPairs(ec2Client)
	if err != nil {
		fmt.Printf("Couldn't fetch key pairs: %v", err)
		return
	}

	fmt.Println("Key Pairs: ")
	for _, pair := range keyPairRes.KeyPairs {
		fmt.Printf("%s \n", *pair.KeyPairId)
	}

	c.IndentedJSON(http.StatusOK, keyPairRes.KeyPairs)
}
func homePage(c *gin.Context) {
	db, e := db.ConnectDB()
	if e != nil {
		log.Fatal(e)
	}
	var ec2lists []ec2Instance
	r, e := db.Query("select id_instance from instance")
	if e != nil {
		log.Fatal(e)
	}
	for r.Next() {
		var ec2list ec2Instance
		e := r.Scan(&ec2list.InstanceId)
		if e != nil {
			log.Fatal(e)
		}
		ec2lists = append(ec2lists, ec2list)
	}
	c.HTML(http.StatusOK, "index.html", gin.H{
		"ec2Instance": ec2lists,
	})
}
func test(c *gin.Context) {
	var ww ec2Instance
	e := c.BindJSON(&ww)
	if e != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request"})
		return
	}
	fmt.Println(ww.InstanceId)
	c.Redirect(http.StatusMovedPermanently, "/")
}

func main() {
	router := gin.Default()
	router.Static("/assets/", "public/")
	router.LoadHTMLGlob("public/view/*.html")

	router.GET("/", homePage)
	router.POST("/api/ec2/create", create)
	router.POST("/api/ec2/test", test)
	router.POST("/api/ec2/terminate", delete)
	router.Run(":8080")
}
